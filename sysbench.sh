#!/bin/bash
apt update && apt install sysbench -y &>/dev/null
sysbench --test=cpu --cpu-max-prime=20000 run
sysbench --test=fileio --file-total-size=5G prepare
sysbench --test=fileio --file-total-size=5G --file-test-mode=rndrw --init-rng=on --max-time=300 --max-requests=0 run
sysbench --test=fileio --file-total-size=5G cleanup