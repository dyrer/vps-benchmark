#!/bin/bash

# Colors
CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"

# Check root access
if [[ "$EUID" -ne 0 ]]; then
	echo -e "${CRED}Sorry, you need to run this as root${CEND}"
	exit 1
fi


wget -qO bench.sh https://git.virtubox.net/virtubox/vps-benchmark/raw/master/bench.sh
chmod +x bench.sh
./bench.sh

# We're done !
echo ""
echo -e "       ${CGREEN}Benchmark successful !${CEND}"
echo ""